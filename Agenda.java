//
// AGENDA
// Alexandre T. K.
//  

import java.util.*;

public class Agenda{

	public static void main(String[] args) {

		Scanner lerTeclado = new Scanner(System.in); //instanciar classe scaner para poder fazer leituras do Teclado.

		Contato umContato; //instanciando classe contato

		ArrayList<Contato> listaContatos = new ArrayList<Contato>(); // Classe padrao, armazena objetos.



		int opcao = -1;
		int i=0;

		while (opcao!=0){
	
			System.out.println();
			System.out.println();
			System.out.println("    1        - Para criar novo contato");
			System.out.println("    2        - Para listar contatos");
			System.out.println("    3        - Para remover um contato");
			System.out.println("    4        - Para pesquisar um contato");
			System.out.println("Outro numero - Para sair do programa");
			opcao = lerTeclado.nextInt();
			lerTeclado.nextLine(); // limpar buffer, o ENTER

			switch(opcao){



		case 1:
//		umContato = new Contato();
	

		System.out.println();
		System.out.println("Digite o nome do Contato:");
		String umNome = lerTeclado.nextLine();
//		umContato.setNome(umNome);
		System.out.println("Digite o telefone do Contato:");
		String umTelefone = lerTeclado.nextLine();
//		umContato.setTelefone(umTelefone);
		umContato = new Contato( umNome, umTelefone);
		System.out.println("Digite o sexo do Contato: ");
		int umSexo = lerTeclado.nextInt();   //ou Integer.parseInt(lerTeclado.nextLine())
		lerTeclado.nextLine(); 
		umContato.setSexo(umSexo);



		listaContatos.add(umContato);



		System.out.println();
		System.out.println();
		System.out.println("CONTATO ADICIONADO");
		System.out.println("*------------------------------------------*");
		System.out.println("Contato numero " + Integer.toString(i+1));
		System.out.println("Nome: " + umContato.getNome());
		System.out.println("Telefone: " + umContato.getTelefone());
		System.out.println();
		if (umContato.getSexo() == 0) {
			System.out.println("Sexo: Masculino");
		} 
		else { 
		if (umContato.getSexo() == 1){
			System.out.println("Sexo: Feminino");
		} 
		else {
			System.out.println("Sexo: não especificado");
		}
		}
		System.out.println("*------------------------------------------*");

		i++;

		break;

	
		case 2:


	for( int j=0; j< listaContatos.size(); j++) {
	
	
		Contato contato = listaContatos.get(j);
		System.out.println();
		System.out.println();
		System.out.println("*------------------------------------------*");
		System.out.println("Contato numero " + Integer.toString(j+1));
		System.out.println("Nome: " + contato.getNome());
		System.out.println("Telefone: " + contato.getTelefone());

		if (contato.getSexo() == 0) {
			System.out.println("Sexo: Masculino");
		} 
		else { 
		if (contato.getSexo() == 1){
			System.out.println("Sexo: Feminino");
		} 
		else {
			System.out.println("Sexo: não especificado");
		}
		}
		System.out.println("*------------------------------------------*");
		System.out.println();
 		}  

		break;


		case 3:
		
		System.out.println("Digite o numero do contato a ser deletado: ");
		
		int opcaoDel = lerTeclado.nextInt();

		listaContatos.remove(opcaoDel-1);

		System.out.println("Contato Deletado.");		

		break;

		case 4:
		boolean encontrado= false;
		System.out.println("Digite o nome do contato");
		String nomePesquisa = lerTeclado.nextLine();
		for( int j=0; j< listaContatos.size(); j++) {
		
		Contato contato= listaContatos.get(j);
		if( nomePesquisa.equals(contato.getNome()) ){
		encontrado= true;
		System.out.println("Contato encontrado! O numero desse contato e: " + 			Integer.toString(j+1));
		break;	
		}		
 		} 
		if (encontrado== false) 
		System.out.println("Contato nao cadastrado");
		 
		break;
		

		default:
		System.out.println();
		System.out.println("Saindo");
		opcao = 0;
	}

}
}
}
